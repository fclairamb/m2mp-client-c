# M2MP C client library

[![Build Status](https://drone.io/github.com/fclairamb/m2mp-client-c/status.png)](https://drone.io/github.com/fclairamb/m2mp-client-c/latest)
[![Build Status](http://ovh5.webingenia.com:10080/api/badges/fclairamb/m2mp-client-c/status.svg)](http://ovh5.webingenia.com:10080/fclairamb/m2mp-client-c)

## Introduction 
This is a simple C implementation of M2MP protocol.

## Current status of the project
The project isn't intended to be used by anyone yet. In the future, I'd like it to be integrated in simple embedded hardware.

## How to compile
This should get you started:

    git clone git://github.com/fclairamb/m2mp-client-c.git
    cd m2mp-client-c
    make run

## The M2MP protocol
Before using this library, you can read the specifications of the [M2MP Protocol](http://florent.clairambault.fr/m2mp-protocol). 


## The missing link
I'd like to create and offer an open-source M2MP platform associated with this library.